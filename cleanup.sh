# Simple cleanup script



IMAGE_NAME=test_tube.img
MACHINE_NAME=test_tube
WORKING_LOCATION=$PWD



BASE_IMAGE=$WORKING_LOCATION/$IMAGE_NAME
VIRSH_CMD="virsh -c qemu:///system"



$VIRSH_CMD shutdown $MACHINE_NAME
$VIRSH_CMD undefine $MACHINE_NAME
rm $BASE_IMAGE
