#!/bin/bash -e

# VM Deployment Platform setup script
# For Libvirt hosts running QEMU on Linux
# Freely available under MIT License
# By Timothy Kenno Handojo

# This program sets up a new copy of disk image
# containing proper Linux system, then define a
# VM instance to run.



source include/user_setting.sh
source include/program_setting.sh

# create disk image
# NOTE: we want the image to be owned by the user
# running this script
dd bs=1M count=4k if=/dev/zero of=$BASE_IMAGE

# create root filesystem
$ROOT_ACCESS "
source include/user_setting.sh
source include/program_setting.sh

source include/root_filesystem.sh
" # end of root stuff

source include/define_vm.sh


