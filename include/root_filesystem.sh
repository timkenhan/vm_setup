# VM Deployment Platform setup script
# For Virsh / Libvirt hosts running QEMU
# By Timothy Kenno Handojo

# Description:
# This script sets up a new copy of disk image
# containing a completely set-up Linux system,
# then define a VM instance to run
#
# Commands for root filesystem setup

# Note that this script is to be exported;
# all double-quotes are to be escaped



# create disk image
modprobe loop
losetup -P $LOOP_DEVICE $BASE_IMAGE
parted -s -a optimal /dev/loop0 mklabel msdos
parted -s -a optimal /dev/loop0 mkpart primary 1 4295
mkfs.ext4 -q -U $ROOT_UUID $LOOP_PARTITION

# mount rootfs
mkdir -p $MOUNT_DIR
mount /dev/loop0p1 $MOUNT_DIR

# install files
tar -C $MOUNT_DIR -xpf $TARBALL_DIR

# install grub
grub-install --boot-directory=$MOUNT_DIR/boot $BASE_IMAGE
sed s/\$UUID/$ROOT_UUID/ -i $MOUNT_DIR/boot/grub/grub.cfg

# modify /etc/hosts
sed s/\$NAME/$MACHINE_NAME/ -i $MOUNT_DIR/etc/hosts

# create /etc/hostname
echo $MACHINE_NAME > $MOUNT_DIR/etc/hostname

# generate ssh pubkey & modify sshd_config
ssh-keygen -t $SSH_KEYTYPE -f $MOUNT_DIR/$SSH_KEYPATH
echo -e $SSH_SETTING >> $MOUNT_DIR/etc/ssh/sshd_config

# modify /etc/network/interfaces
echo -e $NETWORK_SETTING >> $MOUNT_DIR/etc/network/interfaces

# modify /etc/fstab
sed s/\$UUID/$ROOT_UUID/ -i $MOUNT_DIR/etc/fstab
echo -e $EXT_PAR_FST_ENTRY >> $MOUNT_DIR/etc/fstab

# unmount rootfs
umount $MOUNT_DIR
rmdir $MOUNT_DIR
losetup -d /dev/loop0

