# VM Deployment Platform setup script
# For Libvirt hosts running QEMU on Linux
# Freely available under MIT License
# By Timothy Kenno Handojo

# This program sets up a new copy of disk image
# containing proper Linux system, then define a
# VM instance to run.

# Commands for machine definition



cp $ORIGINAL_DEFINITION $TEMP_DEFINITION

sed \
	-e "s@\$MACHINE_NAME@$MACHINE_NAME@" \
	-e "s@\$BASE_IMAGE@$BASE_IMAGE@" \
	-e "s@\$MAX_MEM@$MAX_MEM@" \
	-e "s@\$MAC_ADDRESS@$MAC_ADDRESS@" \
	-e "s@\$EXT_PAR_XML_ENTRY@$EXT_PAR_XML_ENTRY@" \
-i $TEMP_DEFINITION

virsh -c qemu:///system define $TEMP_DEFINITION

rm $TEMP_DEFINITION


