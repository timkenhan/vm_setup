# VM Deployment Platform setup script
# For Virsh / Libvirt hosts running QEMU
# By Timothy Kenno Handojo

# Description:
# This script sets up a new copy of disk image
# containing a completely set-up Linux system,
# then define a VM instance to run
#
# Internal program settings



# temporary variables

MOUNT_DIR=$WORKING_DIR/temp
TEMP_DEFINITION=$WORKING_DIR/temp.xml


# stock files

TARBALL_DIR=$SOURCE_DIR/$SOURCE_TARBALL
ORIGINAL_DEFINITION=$SOURCE_DIR/include/machine.xml
LOOP_DEVICE=/dev/loop0
LOOP_PARTITION=/dev/loop0p1


# stock parameters

if [ "${TARGET_DIR::1}" == "/" ]
then
    BASE_IMAGE=$TARGET_DIR/$IMAGE_NAME
else
    BASE_IMAGE=$PWD/$TARGET_DIR/$IMAGE_NAME
fi

NETWORK_SETTING="\
# appended for $MACHINE_NAME\n\
# main network connection\n\
auto $NET_IFACE\n\
iface $NET_IFACE inet static\n\
    gateway $NET_GATEWAY\n\
    address $NET_ADDRESS\n\
    netmask 255.255.255.0\n\
$NET_EXTRA
" # end of NETWORK_SETTINGS

SSH_SETTING="\
# appended for $MACHINE_NAME\n\
Port $SSH_PORT\n\
HostKey $SSH_KEYPATH\n\
" # end of SSH_SETTING
   
if [ ! -z $EXT_PAR_LOC ]
then
EXT_PAR_FST_ENTRY="/dev/sdb\
    $EXT_PAR_MNT \
    $EXT_PAR_TYPE \
    defaults 0 2
" # end of EXT_PAR_FST_ENTRY
EXT_PAR_XML_ENTRY="\
    <disk type='file' device='disk'>\n\
      <driver name='qemu' type='raw'/>\n\
      <source file='$EXT_PAR_LOC'/>\n\
      <target dev='hdb' bus='ide'/>\n\
    </disk>\n\
" # end of EXT_PAR_XML_ENTRY
else
EXT_PAR_FST_ENTRY=""
EXT_PAR_XML_ENTRY=""
fi


