# VM Deployment Platform setup script
# For Virsh / Libvirt hosts running QEMU
# By Timothy Kenno Handojo

# Description:
# This script sets up a new copy of disk image
# containing a completely set-up Linux system,
# then define a VM instance to run
#
# User settings



# basic machine settings -- CHANGE THESE!

MACHINE_NAME=NONAME
IMAGE_NAME=NONAME.img
SOURCE_TARBALL=base-clean_debian-9.tar.xz
SOURCE_DIR=$PWD
TARGET_DIR=$HOME
WORKING_DIR=/tmp
NET_GATEWAY=192.168.2.1
NET_ADDRESS=192.168.2.22


# network connection

NET_IFACE=ens2
# NET_GATEWAY -- defined above
# NET_ADDRESS -- defined above
NET_EXTRA="" # extra stuff, e.g. DNS


# SSH access

SSH_PORT=22
SSH_KEYTYPE=ed25519
SSH_KEYPATH=/etc/ssh/ssh_host_ed25519_key


# external partition

EXT_PAR_LOC= # empty, valid, or no run!
EXT_PAR_MNT=/srv
EXT_PAR_TYPE=ext4


# other settings

MAX_MEM=1 # in GiB
MAC_ADDRESS="52:54:00:22:4a:e7"
ROOT_UUID="99b17096-adf8-4f17-84ac-92794e6403f1"
ROOT_ACCESS="su -c"


