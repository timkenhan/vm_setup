# VM Deployment Platform setup script
# For Libvirt hosts running QEMU on Linux
# Freely available under MIT License
# By Timothy Kenno Handojo

# This program sets up a new copy of disk image
# containing proper Linux system, then define a
# VM instance to run.



# NOTE:
# Append the network interface entry below to /etc/network/interface
# Replace:
#   - <inet_device> : device interface that connect to the network
#   - <bridge_device> : device interface that bridges
#   - <host_ip> : host's network IP address
#   - <inet_ip> : gateway IP address to the network
# This is assuming that you're using Debian or Ubuntu based system.

# The primary network interface
iface <inet_device> inet manual
auto <bridge_device>
iface <bridge_device> inet static
    address <host_ip>
    gateway <inet_ip>
    netmask 255.255.255.0
    bridge_ports <inet_device>
    bridge_waitport 0
    bridge_maxwait 0
    bridge_stp off
    bridge_fd 0

# example network interface
#iface eth0 inet manual
#auto br0
#iface br0 inet static
#    address 192.168.2.2
#    gateway 192.168.2.1
#    netmask 255.255.255.0
#    bridge_ports eth0
#    bridge_waitport 0
#    bridge_maxwait 0
#    bridge_stp off
#    bridge_fd 0

